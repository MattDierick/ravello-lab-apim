.. API security and mgmt documentation master file, created by
   sphinx-quickstart on Thu Oct 10 11:39:16 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to API Management and Security Lab
==========================================

.. warning :: For any remark or mistake in this lab, please send ASAP a Teams chat to Matthieu DIERICK, Graham ALDERSON and Kevin REYNOLDS so that we can fix it for the next sessions.

.. note :: Contributors : CJ CHEN, David ARTHUR, Shahnawaz BACKER, Sudarshan SIVAPERUMAL, Philippe CLOUP

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   class*/class*

